#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#define CL_TARGET_OPENCL_VERSION 220
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl.h>

int main(int argc, char *argv[]) {
    cl_command_queue command_queue;
    cl_context context;
    cl_device_id device;
    cl_int input = 1;
    cl_int kernel_result = 0;
    cl_int call_result;
    cl_kernel kernel;
    cl_mem buffer;
    cl_platform_id platform;
    cl_program program;
    const char *source = "__kernel void increment(int in, __global int* out) { out[0] = in + 1; }";
    (void)argc;

    call_result = clGetPlatformIDs(1, &platform, NULL);
    if (call_result != CL_SUCCESS) {
        fprintf(stderr, "%s: clGetPlatformIDs returned %d\n", argv[0], (int)call_result);
        exit(1);
    }
    call_result = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 1, &device, NULL);
    if (call_result != CL_SUCCESS) {
        fprintf(stderr, "%s: clGetDeviceIDs returned %d\n", argv[0], (int)call_result);
        exit(1);
    }
    context = clCreateContext(NULL, 1, &device, NULL, NULL, NULL);
    command_queue = clCreateCommandQueue(context, device, 0, NULL);
    buffer = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, sizeof(cl_int), NULL, NULL);
    program = clCreateProgramWithSource(context, 1, &source, NULL, NULL);
    clBuildProgram(program, 1, &device, "", NULL, NULL);
    kernel = clCreateKernel(program, "increment", NULL);
    clSetKernelArg(kernel, 0, sizeof(cl_int), &input);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), &buffer);
    clEnqueueTask(command_queue, kernel, 0, NULL, NULL);
    clFlush(command_queue);
    clFinish(command_queue);
    clEnqueueReadBuffer(command_queue, buffer, CL_TRUE, 0, sizeof (cl_int), &kernel_result, 0, NULL, NULL);

    assert(kernel_result == 2);
    return EXIT_SUCCESS;
}
